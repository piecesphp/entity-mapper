<?php
header('Content-Type: text/html');
error_reporting(E_ALL);
ini_set('display_errors', true);
require_once __DIR__ . '/crear_registros.php';
require_once __DIR__ . '/EmpleadoMapper.php';
try {
    echo "<pre><strong>";
    $dbname = __DIR__ . '/testdb.sq3';//Nombre de la base de datos
    $options_connection = ['driver' => 'sqlite', 'database' => $dbname];//Configuración de la conexión

    EmpleadoMapper::setOptions($options_connection);//Establecer las configuraciones

    $mapper = new EmpleadoMapper(); //Mapeador vacío
    $mapper1 = new EmpleadoMapper(5); //Mapeador con parámetro para llenar los valores

    echo json_encode($mapper->humanReadable(), \JSON_PRETTY_PRINT) . "<br>";
    echo json_encode($mapper1->humanReadable(), \JSON_PRETTY_PRINT) . "<br>";

    $all = array_map(
        function ($e) {
            return $e->humanReadable();
        },
        $mapper->getAll()
    );
    //var_dump($all);
    echo "</strong></pre>";


} catch (\Exception $e) {
    var_dump($e);
}