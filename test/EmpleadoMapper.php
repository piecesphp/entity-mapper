<?php

use \PiecesPHP\Core\Database\EntityMapper;

require_once __DIR__ . '/../src/Core/Database/EntityMapper.php';
require_once __DIR__ . '/CargoMapper.php';

class EmpleadoMapper extends EntityMapper
{

    protected $table = 'empleados';
    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'name' => [
            'type' => 'varchar',
        ],
        'cargo' => [
            'type' => 'int',
            'reference_table' => 'cargos',
            'reference_field' => 'id',
            'human_readable_reference_field' => 'name',
            'hasMany' => false,
            'mapper' => CargoMapper::class,
        ],
    ];

    public function __construct(int $value_compare = null, string $field_compare = 'primary_key', $options = null)
    {
        parent::__construct($value_compare, $field_compare, $options);
    }
}