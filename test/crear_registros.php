<?php

use \PiecesPHP\Core\Database\ActiveRecordModel;

require_once __DIR__ . '/../vendor/autoload.php';

$dbname = __DIR__ . '/testdb.sq3';

if (!file_exists($dbname)) {

    $options_connection = ['driver' => 'sqlite', 'database' => $dbname];
    $sql = file_get_contents(__DIR__ . '/sql.sql');

    $model = new ActiveRecordModel($options_connection);

    $pdo = $model->getDb();
    $pdo->exec($sql);

    echo "<strong>Insertando cargos:</strong>";
    $model->setTable('cargos');
    $insertados = 0;
    for ($i = 0; $i < 50; $i++) {
        if ($model->insert(['name' => 'Cargo: ' . rand(0, 100)])->execute())
            $insertados++;
    }
    echo " <strong>$insertados cargos insertados.</strong><br>";

    echo "<strong>Insertando empleados:<strong>";
    $model->setTable('empleados');
    $insertados = 0;
    for ($i = 0; $i < 50; $i++) {
        if ($model->insert(['name' => 'Empleado: ' . rand(0, 100), 'cargo' => $insertados + 1])->execute())
            $insertados++;
    }
    echo " <strong>$insertados empleados insertados. </strong><br>";

}