<?php

use \PiecesPHP\Core\Database\EntityMapper;

require_once __DIR__ . '/../src/Core/Database/EntityMapper.php';

class CargoMapper extends EntityMapper
{

    protected $table = 'cargos';
    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'name' => [
            'type' => 'varchar',
        ],
    ];

    public function __construct(int $value_compare = null, string $field_compare = 'primary_key', $options = null)
    {
        parent::__construct($value_compare, $field_compare, $options);
    }
}